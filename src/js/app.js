require('../css/app.css');
require('core-js/stable');
require('regenerator-runtime/runtime');
const teachersModule = require('./teachers');

/** ******** Your code here! *********** */

let allTeachers = new Map();
const favourites = new Set();
let query = {};
let sort = null;

const favIcon = new DOMParser()
  .parseFromString('<img class="icon" src="https://img.icons8.com/fluency/48/000000/star.png" alt="Favourite"/>',
    'text/html').body.childNodes[0];

function renderTeacherCard(teacher) {
  const card = document.createElement('div');
  card.classList.add('teacher-card');
  const name = document.createElement('a');
  name.classList.add('name');
  name.textContent = teacher.full_name;
  name.onclick = () => openTeacherModal(teacher);
  const country = document.createElement('p');
  country.classList.add('country');
  country.textContent = teacher.country;
  const image = document.createElement('img');
  image.classList.add('avatar');
  image.src = teacher.picture_thumbnail
    ?? 'https://e7.pngegg.com/pngimages/178/595/png-clipart-user-profile-computer-icons-login-user-avatars-monochrome-black-thumbnail.png';
  image.alt = 'Teacher\'s photo';

  card.append(image, name, country);

  if (favourites.has(teacher.id)) {
    card.insertBefore(favIcon.cloneNode(), name);
  }

  return card;
}

function renderTableRow(teacher) {
  const tr = document.createElement('tr');
  const name = document.createElement('td');
  name.textContent = teacher.full_name;
  const age = document.createElement('td');
  age.textContent = teacher.age;
  const gender = document.createElement('td');
  gender.textContent = teacher.gender;
  const country = document.createElement('td');
  country.textContent = teacher.country;

  tr.append(name, age, gender, country);
  return tr;
}

function getFilteredTeachers() {
  const filterByPhotos = document.getElementById('show-photos').checked;
  const filterByFavourites = document.getElementById('show-favourites').checked;
  const search = query.search?.toLowerCase();
  const age = parseInt(query.age, 10);

  const teachers = [];
  for (const teacher of allTeachers.values()) {
    const applyPhotoFilter = !filterByPhotos || teacher.picture_thumbnail;
    const applyFavouriteFilter = !filterByFavourites || favourites.has(teacher.id);
    const applySearchFilter = !search
      || teacher.full_name?.toLowerCase()
        .includes(search)
      || teacher.age?.toString()
        .includes(search)
      || teacher.note?.toLowerCase()
        .includes(search);
    const applyAgeFilter = !age || teacher.age === age;

    if (applyPhotoFilter && applyFavouriteFilter && applySearchFilter && applyAgeFilter) {
      teachers.push(teacher);
    }
  }
  return teachers;
}

function refreshTopTeachers() {
  const cards = getFilteredTeachers()
    .map(renderTeacherCard);
  const el = document.getElementById('top-teachers');
  el.innerHTML = '';
  el.append(...cards);
}

function refreshFavourites() {
  const ids = Array.from(favourites.values());
  const cards = ids.map((id) => renderTeacherCard(allTeachers.get(id)));
  const el = document.getElementById('favourite-teachers');
  el.innerHTML = '';
  el.append(...cards);
}

function updateTableSortHeaders() {
  document.querySelectorAll('.sortable')
    .forEach((el) => {
      // Iterate over all header cells and put arrow on currently sorted column
      // and remove it from previously sorted one
      if (sort && sort.field === el.dataset.sort) {
        if (sort.direction === 'asc') {
          el.classList.add('headerSortUp');
        } else {
          el.classList.remove('headerSortUp');
          el.classList.add('headerSortDown');
        }
      } else {
        el.classList.remove('headerSortUp');
        el.classList.remove('headerSortDown');
      }
    });
}

function refreshTable() {
  updateTableSortHeaders();
  const teachers = sort
    ? teachersModule.sortUsers(getFilteredTeachers(), [sort])
    : getFilteredTeachers();
  const rows = teachers.map(renderTableRow);
  const el = document.querySelector('#statistics > table > tbody');
  el.innerHTML = '';
  el.append(...rows);
}

function updatePageState() {
  const params = new URLSearchParams(window.location.search);
  query = {
    search: params.get('search'),
    age: params.get('age'),
    gender: params.get('gender'),
    nationality: params.get('nationality'),
  };
  document.querySelector('#search').value = query.search;
  document.querySelector('#age-search').value = query.age;
  document.querySelector('#gender').value = query.gender;
  document.querySelector('#nationality').value = query.nationality;
}

async function fetchTeachers(additional) {
  const teachers = await teachersModule.getTeachers(query, additional);
  if (additional) {
    teachers.forEach((t) => allTeachers.set(t.id, t));
  } else {
    allTeachers = new Map(teachers.map((t) => [t.id, t]));
  }
}

function refreshAll() {
  refreshTopTeachers();
  refreshTable();
  refreshFavourites();
}

function toggleFavourite(teacher) {
  if (favourites.has(teacher.id)) {
    favourites.delete(teacher.id);
  } else {
    favourites.add(teacher.id);
  }

  document.querySelector('.is-fav').src = favourites.has(teacher.id)
    ? 'https://img.icons8.com/fluency/48/000000/star.png'
    : 'https://img.icons8.com/color/48/000000/star--v1.png';

  refreshTopTeachers();
  refreshFavourites();
}

function openTeacherModal(teacher) {
  const modal = document.getElementById('teacher-info');
  modal.querySelector('.teacher-name').textContent = teacher.full_name;
  modal.querySelector('.city').textContent = teacher.city;
  modal.querySelector('.country').textContent = teacher.country;
  modal.querySelector('.gender').textContent = teacher.gender;
  modal.querySelector('.age').textContent = teacher.age;
  modal.querySelector('.email').textContent = teacher.email;
  modal.querySelector('.phone').textContent = teacher.phone;
  modal.querySelector('.comment').textContent = teacher.note;
  modal.querySelector('.avatar-full').src = teacher.picture_large
    ?? 'https://e7.pngegg.com/pngimages/178/595/png-clipart-user-profile-computer-icons-login-user-avatars-monochrome-black-thumbnail.png';
  modal.querySelector('.is-fav').src = favourites.has(teacher.id)
    ? 'https://img.icons8.com/fluency/48/000000/star.png'
    : 'https://img.icons8.com/color/48/000000/star--v1.png';

  modal.querySelector('.fav-btn').onclick = () => toggleFavourite(teacher);
  modal.classList.remove('hidden');
}

function openAddTeacherModal() {
  const modal = document.getElementById('add-teacher');
  modal.querySelector('#fullName').value = '';
  modal.querySelector('#age').value = '';
  modal.querySelector('#email').value = '';
  modal.querySelector('#phone').value = '';
  modal.querySelector('#background').value = '';
  modal.querySelector('#country').value = '';
  modal.querySelector('#city').value = '';
  modal.querySelector('#comment').value = '';

  modal.classList.remove('hidden');
}

function addTeacher() {
  const formData = new FormData(document.querySelector('form[name="add-teacher"]'));
  const teacher = {
    id: teachersModule.guidGenerate(),
    full_name: formData.get('fullName'),
    age: parseInt(formData.get('age'), 10),
    email: formData.get('email'),
    gender: formData.get('gender'),
    phone: formData.get('phone'),
    bg_color: formData.get('background'),
    country: formData.get('country'),
    city: formData.get('city'),
    note: formData.get('note'),
  };

  console.log(teacher);

  if (teachersModule.isValidUser(teacher)) {
    fetch('http://localhost:3005/users', {
      method: 'post',
      body: JSON.stringify(teacher),
      headers: { 'Content-Type': 'application/json' },
    })
      .then(() => {
        allTeachers.set(teacher.id, teacher);
        refreshAll();
        document.getElementById('add-teacher')
          .classList
          .add('hidden');
      });
    document.querySelector('#user-valid')
      .classList
      .add('hidden');
  } else {
    document.querySelector('#user-valid')
      .classList
      .remove('hidden');
  }
}

// Add event listeners to elements
document.getElementById('show-photos').onchange = refreshAll;
document.getElementById('show-favourites').onchange = refreshAll;
document.querySelector('form[name="search"]').onsubmit = (event) => {
  event.preventDefault();
  const form = event.currentTarget;
  const formData = new FormData(form);
  const params = new URLSearchParams();
  for (const [key, value] of formData.entries()) {
    if (value) {
      params.append(key, value);
    }
  }
  window.history.pushState(null, '', `?${params}`);
  onQueryUpdate();
};
document.querySelectorAll('.close-popup')
  .forEach((el) => el.addEventListener('click', (event) => {
    const popup = event.target.parentNode;
    popup.classList.add('hidden');
  }));
document.querySelectorAll('.add-teacher-btn')
  .forEach((el) => el.addEventListener('click', openAddTeacherModal));

document.querySelector('form[name="add-teacher"]').onsubmit = (event) => {
  event.preventDefault();
  addTeacher();
};

document.querySelectorAll('.sortable')
  .forEach((el) => el.addEventListener('click', (event) => {
    const header = event.target;
    const field = header.dataset.sort;
    if (sort && sort.field === field) {
      if (sort.direction === 'asc') {
        sort.direction = 'desc';
      } else {
        sort = null;
      }
    } else {
      sort = {
        field,
        direction: 'asc',
      };
    }
    refreshTable();
  }));

document.querySelector('#load-more')
  .addEventListener('click', () => {
    fetchTeachers(true)
      .then(refreshAll);
  });

function onQueryUpdate() {
  updatePageState();
  fetchTeachers()
    .then(refreshAll);
}

window.onpopstate = onQueryUpdate;
// Initial refresh of page content
onQueryUpdate();

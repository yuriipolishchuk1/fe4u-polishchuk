const {
  additionalUsers,
  randomUserMock,
} = require('./mock_for_L3');

/* eslint-disable no-console */

function guidGenerate() {
  const S4 = () => (((1 + Math.random()) * 0x10000) | 0).toString(16)
    .substring(1);
  return (`${S4()}${S4()}-${S4()}-${S4()}-${S4()}-${S4()}${S4()}${S4()}`);
}

function uniqueBy(a, key) {
  const seen = new Set();
  const res = [];
  for (const elem of a) {
    if (!seen.has(key(elem))) {
      res.push(elem);
      seen.add(key(elem));
    }
  }
  return res;
}

function normalizeUser(user) {
  return {
    gender: user.gender ?? null,
    title: user.name?.title ?? null,
    full_name: user.name ? `${user.name.first} ${user.name.last}` : null,
    city: user.location?.city ?? null,
    state: user.location?.state ?? null,
    country: user.location?.country ?? null,
    postcode: user.location?.postcode ?? null,
    coordinates: user.location?.coordinates ?? null,
    timezone: user.location?.timezone ?? null,
    email: user.email ?? null,
    b_date: user.dob?.date ?? null,
    age: user.dob?.age ?? null,
    phone: user.phone ?? null,
    picture_large: user.picture?.large ?? null,
    picture_thumbnail: user.picture?.thumbnail ?? null,
    id: user.id?.name && user.id?.value ? user.id.name + user.id.value : guidGenerate(),
    course: null,
    bg_color: '#1f75cb',
    note: 'Some person',
  };
}

function normalize() {
  const users = randomUserMock.map(normalizeUser);

  users.push(...additionalUsers);

  return uniqueBy(uniqueBy(users, (u) => u.id), (u) => u.full_name);
}

async function getTeachers(query, additional) {
  const params = new URLSearchParams();
  params.append('results', additional ? 10 : 50);
  if (query.gender) {
    params.append('gender', query.gender);
  }
  if (query.nationality) {
    params.append('nationality', query.nationality);
  }
  const resp = await fetch(`https://randomuser.me/api?${params}`);
  const json = await resp.json();
  return json.results.map(normalizeUser);
}

function isValidUser(user) {
  const validStr = (str) => typeof str === 'string' && str[0] === str[0].toUpperCase();
  const verifyStrings = (...strs) => strs.every(validStr);
  const verifyAge = (age) => typeof age === 'number';
  const verifyEmail = (email) => /^\S+@\S+\.\S+$/.test(email);
  const verifyPhone = (phone) => /^[+]?[(]?[0-9]{3}[)]?[-\s.]?[0-9]{3}[-\s.]?[0-9]{4,6}$/.test(phone);

  return verifyStrings(user.full_name, user.gender, user.note, user.city, user.country)
    && verifyAge(user.age)
    && verifyEmail(user.email)
    && verifyPhone(user.phone);
}

function sortUsers(users, sort) {
  const sortFunc = (u1, u2) => {
    for (const {
      field,
      direction,
    } of sort) {
      if (u1[field] < u2[field]) return direction === 'asc' ? -1 : 1;
      if (u1[field] > u2[field]) return direction === 'asc' ? 1 : -1;
    }
    return 0;
  };
  return users.sort(sortFunc);
}

function findUser(users, match) {
  for (const user of users) {
    let isMatch = true;
    for (const [field, value] of Object.entries(match)) {
      if (user[field] !== value) {
        isMatch = false;
        break;
      }
    }
    if (isMatch) return user;
  }
  return null;
}

function userPercentage(users, predicate) {
  let count = 0;
  for (const user of users) {
    if (predicate(user)) count += 1;
  }
  const proportion = count / users.length;
  return proportion * 100;
}

module.exports = {
  findUser,
  normalize,
  userPercentage,
  sortUsers,
  isValidUser,
  guidGenerate,
  getTeachers,
};
